% -------------------------------------------------------------
% Universidad de Alicante  
% -------------------------------------------------------------
% PROYECTO	: Phd Thesis José Carlos Rangel Ortiz
% ARCHIVO	: 06_conclusions.tex 
% AUTOR		: Jose Carlos Rangel Ortiz [jcrangel]		
% FECHA		: 11/01/2016
% -------------------------------------------------------------
% Conclusions and future work chapter  
% -------------------------------------------------------------

\chapter{Conclusions}
\label{cap:conclusiones}

\selectlanguage{english}


\shadowbox{
\begin{minipage}{0.9\textwidth}
	This chapter discusses the main conclusions extracted from the work presented in this dissertation. The chapter is organized in four sections: Section \ref{cap:conclusions:conclusions} presents and discusses the final conclusions of the work presented in this thesis. Section \ref{cap:conclusions:contributions} enumerates the most relevant contributions made in the topic of research. Next, Section \ref{cap:conclusions:publications} lists the publications derived from the presented work. Finally, Section \ref{cap:conclusions:future} presents future works: open problems and research topics that remain for future research. 
\end{minipage}
}

\section{Conclusions}
\label{cap:conclusions:conclusions}

In this thesis we focus on solving scene understanding problems for mobile robots. These kinds of problems are usually addressed by means of object recognition procedures. Hence, we first presented a study for testing how to manage noise in 3D clouds in order to accurately identify objects. In this study, Growing Neural Gas (GNG) was used to represent noisy clouds which were then compared with other 3D representation methods. However, although GNG produced positive results, 3D object recognition still had to deal with several problems as well as computational requirements. Consequently, we decided to explore the possibility of using 2D data in the procedure.

Furthermore, after studying 2D recognition approaches, we came up with a novel method to represent images, based on semantic labels   produced with a Convolutional Neural Network (CNN) model. These semantic labels also include a confidence value representing how likely it is for the label to be present in the image. These pairs of values (label-confidence) was used as a descriptor for images. The proposed method employs deep learning techniques, but it does not need long training periods in order to build the classification/labeling models. Therefore, the proposed method could be adapted to any labeling systems that produce a label with a corresponding confidence value.

The semantic descriptor allows us to work with images and obtain measures for image comparison in order to group them by similarity. Using this descriptor, we could infer semantic information from an image or a group of images. This semantic information could describe the composition of the images (object presence) as well as their probable semantic category (room, kitchen). Therefore, semantic descriptors contribute not only as a regular image representation, but also with their semantic description capability.

In addition, we evaluated the semantic descriptor in supervised indoors scene classification problems. Results demonstrate the goodness of the semantic descriptor when compared with traditional numeric descriptors.   

Moreover, the  semantic descriptor was tested in Topological and Semantic Mapping issues. Here, the use of an accurate image representation makes the constructed maps robust. In mapping situations the semantic descriptor supplies a wide description of possible objects present in the room. In addition, the category of places for semantic mapping could be derived only by taking into account the labels with highest mean confidence, obtained by grouping similar images in a sequence. Similarly, this inference procedure could be applied to topological maps where the groups correspond to nodes of similar images in a sequence.

Last but not least, CNN models have been tested as a labeling system for clusters of points segmented from point clouds, in order to train a classifier that could accurately recognize 3D objects that would normally be difficult to detect when using 2D image classifiers.

Finally, the studies carried out in this thesis allow us to confirm that using  semantic descriptors produced by some labeling external system (in our case, a CNN model) makes it possible to achieve detailed understanding of the scenes as well as their semantic meaning.

\section{Contributions of the Thesis}
\label{cap:conclusions:contributions}

The contributions made in this body of research are as follows:

\begin{itemize}
	
	\item The application of the GNG-based algorithm to represent noisy scene  and develop object recognition in these scenes. 
	\item The use of pre-trained CNN models as a labeling system to build a semantic image descriptor.
	\item The application of a semantic descriptor in mapping procedures in order to infer semantic information about places.
	\item The training of 3D objects classifiers with objects labeled using a 2D pre-trained CNN model.
\end{itemize}

\section{Publications}
\label{cap:conclusions:publications}

The following articles were published as a result of the research carried out by the doctoral candidate:

\begin{itemize}
\item Published articles in scientific journals:
    \begin{itemize}
     \item José Carlos Rangel, Jesús Martínez-Gómez, Cristina Romero-González, Ismael García-Varea and  Miguel Cazorla: OReSLab: 3D Object Recognition through CNN Semi-Supervised Labeling. Under Review. \textbf{Applied Soft Computing} Impact Factor (JCR 2016): 3.541.
    \newline
     \item José Carlos Rangel, Miguel Cazorla, Ismael García-Varea, Cristina Romero-González and Jesús Martínez-Gómez: AuSeMap: Automatic Semantic Map Generation. Under Review. \textbf{Autonomous Robots} Impact Factor (JCR 2016): 2.706.
    \newline
    \item José Carlos Rangel, Jesús Martínez-Gómez, Ismael García-Varea and Miguel Cazorla: LexToMap: Lexical-based Topological Mapping. \textbf{Advanced Robotics} 31(5):268-281 (2017) Impact Factor (JCR 2016): 0.920.
    \newline
    \item José Carlos Rangel, Miguel Cazorla, Ismael García-Varea, Jesús Martínez-Gómez, Elisa Fromont and Marc Sebban: Scene Classification based on Semantic Labeling. \textbf{Advanced Robotics} 30(11-12):758-769 (2016) Impact Factor (JCR 2016): 0.920.
    \newline
    \item José Carlos Rangel, Vicente Morell, Miguel Cazorla, Sergio Orts-Escolano and José García-Rodríguez: Object recognition in noisy RGB-D data using GNG. \textbf{Pattern Analysis and Applications} Accepted (2016) Impact Factor (JCR 2016): 1.352.
    \end{itemize}
\clearpage
\item International conferences:
    \begin{itemize}
    \item José Carlos Rangel, Miguel Cazorla, Ismael García-Varea, Jesús Martínez-Gómez, Élisa Fromont and Marc Sebban: \textbf{Computing Image Descriptors from Annotations Acquired from External Tools.} Robot 2015: Second Iberian Robotics Conference, Lisbon, Portugal:  673-683, November 19-21.
    \newline
    \item José Carlos Rangel, Vicente Morell, Miguel Cazorla, Sergio Orts-Escolano and José García-Rodríguez: \textbf{Using gng on 3d object recognition in noisy rgb-d data.} International Joint Conference on Neural Networks, IJCNN 2015, Killarney, Ireland, July 12-17. CORE A.
    \newline
    \item José Carlos Rangel, Vicente Morell, Miguel Cazorla, Sergio Orts-Escolano and José García-Rodríguez: \textbf{Object Recognition in Noisy RGB-D Data.} International work-conference on the ınterplay between natural and artificial computation, IWINAC 2015, Elche, Spain: 261-270, June 1-5. CORE B.
    \newline
    \end{itemize}

\end{itemize}

\section{Future Work}
\label{cap:conclusions:future}

As a future work we propose the integration of the developed semantic classification methods in a mobile robotic platform in order to achieve a mobile platform able to analyze places in real time, which could be used in home assistance.

Regarding the semantic descriptor, we plan to continue studying its  descriptive capabilities by selecting labels with high probability values and generating a reduced version of the descriptor. This procedure will take into account the actual category of the image for label selection.

Moreover, we propose the use of several models in conjunction; in other words, the application of several models to the same set of images. Each model would be focus on different aspects, such as scenes, objects, instruments, furniture, etc., with the objective of obtaining the semantic classification of a place as well as its possible content. This approach could strengthen the scene understanding of a specific place.

We also propose the study of the faculties that the semantic descriptor has, in order to use them in outdoor scenes, where they could face challenges such as a greater perspective of the environment, greater number of elements in the scenes, different types and sources of illumination, as well as elements whose size is imperceptible for the neural network.

For the generation of semantic descriptors we propose the use of novel designs of CNN architectures, such as ResNet and other pre-trained models and frameworks such as Keras with TensorFlow, to complete a comparative study of these models with the ones used in this thesis and achieve an evaluation metrics that will eventually lead to the selection of the model that best fits each kind of problem.
