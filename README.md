SUMRExDeL
----------------------------------

Document with the thesis latex sources
Content
Booklet folder: contains the .tex document for printing the tesis document in an A5 format in a pocket book version.
Portadas Folder: contains the Photoshop PDF version of the covers of the tesis and also the standard EDUA cover.

Contact information: jcrangel@dccia.ua.es @jcarlos2289
